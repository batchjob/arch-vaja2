#!/usr/bin/env python3
""" DECODER """

import sys
from collections import deque
from conversion import *

# "c0", "c1",

FORBIDDEN_TUPLE = ("f5","f6","f7","f8","f9","fa","fb","fc","fd","fe","ff")
FORBIDDEN_TUPLE = FORBIDDEN_TUPLE + tuple(c.upper() for c in FORBIDDEN_TUPLE)
TOO_MUCH_BYTES = ("C0", "C1", "c0", "c1")

if __name__ == "__main__":


    sys.stdout.write("Enter number: ")
    sys.stdout.flush()


    test_n = sys.stdin.readline().strip().replace(' ','')

    prefix_check = test_n[0:2]
    number = None
    if(prefix_check == "0b" or prefix_check == "0B"):
        if(isBin(test_n)):
            print("Number in bin format")
            number = binStringToNumber(test_n[2:])
            hex_string = binStringToHexString(test_n[2:])
        else:
            print("Invalid bin format")
            sys.exit(1)

    elif(prefix_check == "0X" or prefix_check == "0x"):
        if(isHex(test_n)):
            print("Number in hex format")
            number = hexStringToNumber(test_n[2:])
            hex_string = test_n[2:]
        else:
            print("Invalid hex format")
            sys.exit(1)
    # Try to auto parse
    else:
        if(isBin(test_n)):
            print("Number is bin")
            number = binStringToNumber(test_n)
            hex_string = binStringToHexString(test_n)
        elif(isHex(test_n)):
            print("Num is hex")
            number = hexStringToNumber(test_n)
            hex_string = test_n
        else:
            print("Number entered is not in valid hex or bin format")
            sys.exit(1)
    
    if(number is None):
        print("Internal error")
        sys.exit(-1)
    
    # print(hex_string)
    # print(number)

    if(number < 128):
        print("D: {}".format(number))
        __binstring = numberToBin(number)
        print("B: {}".format(__binstring))
        print("H: {}".format(binStringToHexString(__binstring)))
        sys.exit(0)
    
    if(hex_string[:2] in FORBIDDEN_TUPLE):
        print("First byte is wrong")
        sys.exit(2)

    if(hex_string[:2] in TOO_MUCH_BYTES):
        print("char was encoded with too many bytes")
        # sys.exit(2) 


    binary_string = [char for char in numberToBin(number)]
    dd=deque(binary_string)

    n_bytes=0
    i=0
    while(dd.popleft() == "1"):
        n_bytes=n_bytes+1
    i=n_bytes+1

    byte_buffer=list()
    if(n_bytes*2 != len(hex_string)):
        print("Invalid size od input")
        sys.exit(2)    
    
    while(len(dd) > 0):
        item = dd.popleft()

        if(i % 8 == 0):
            item2 = dd.popleft()
            if(item != "1" or item2 != "0"):
                print("Invalid sequence, exiting")
                sys.exit(1)
            i=i+1
        else:
            byte_buffer.append(item)

        i=i+1


    final_bin="".join(byte_buffer)
    print("H: 0x{}".format(binStringToHexString(final_bin)))
    print("B: 0b{}".format(final_bin))
    __final_number = binStringToNumber(final_bin)

    if(__final_number > 33 and __final_number != 127):
        print("S: {}".format(chr(__final_number)))
    else:
        print("S: {}".format(__final_number))

    sys.exit()