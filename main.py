#/usr/bin/env python3
# http://www.ltg.ed.ac.uk/~richard/utf-8.cgi?input=1235&mode=decimal
# for stdin, stdout
import sys

BIN_CHARS = ("0", "1")
DEC_CHARS = BIN_CHARS + ("2" ,"3" ,"4" ,"5" ,"6" ,"7", "8", "9")
__HEX_CHARS = ("A", "B", "C", "D", "E", "F")
HEX_CHARS = DEC_CHARS + __HEX_CHARS + tuple(i.lower() for i in __HEX_CHARS)

BIN = 2
HEX = 16
DEC = 10

from conversion import *

__DEBUG = True

def printD(data):
    if(__DEBUG):
        print(data)

def isHex(string):
    # TODO optimization
    __string = string
    
    if(string[0:2] == "0x" or string[0:2] == "0X"):
        __string = string[2:]

    for l in __string:
        if (l not in HEX_CHARS):
            return False

    return True

def isBin(string):
    if(string[0:2] == "0b" or string[0:2] == "0b"):
        __string = string[2:]
    else:
        __string = string

    for c in __string:
        if(c not in BIN_CHARS):
            return False
    
    return True

def isDec(string):
    for c in string:
        if(c not in DEC_CHARS):
            return False
    
    return True

def inputCheck(string):
    printD("Checking input for {}".format(string))
    if(isDec(string)):
        printD("{} is DEC".format(string))
        if(isBin(string)):
            print("To input as bin, prefix your number with \"0b\"")
        return DEC
    if(isBin(string)):
        printD("{} is BIN".format(string))
        return BIN
    if(isHex(string)):
        printD("{} is HEX".format(string))
        return HEX

    raise ValueError("Number not in bin, hex or dec format")


def baseCheck(number):
    if(number < 127):
        return 1
    if(number < 2048):
        return 2
    if(number < 65565):
        return 3
    if(number > 0x10FFFF):
        raise ValueError("Number too high for UTF-8")
    
    return 4


    

""" UNIT TESTS FOR FUNCTIONS"""
def hexUnit():
    okList = ("0A5", "0x15AF", "0X515BAF", "aBf17")
    badList = ("xafb", "DABFG", "klow0B835412")

    for item in okList:
        try:
            assert(isHex(item))
        except AssertionError:
            print("Assertion failed for hex at: {}".format(item))

    for item in badList:
        try:
            assert(not isHex(item))
        except AssertionError:
            print("Assertion failed for hex at: {}".format(item))

def binUnit():
    okList = ("1001010", "00000010110", "10011101010", "0110")
    badList = ("105", "qw1", "019581s")

    for item in okList:
        try:
            assert(isBin(item))
        except AssertionError:
            print("Assertion failed for bin at: {}".format(item))

    for item in badList:
        try:
            assert(not isBin(item))
        except AssertionError:
            print("Assertion failed for bin at: {}".format(item))

def decUnit():
    okList = ("1052", "5777521", "141057", "001410458")
    badList = ("105,", "qw1", "019581s")
    for item in okList:
        try:
            assert(isDec(item))
        except AssertionError:
            print("Assertion failed for dev at: {}".format(item))

    for item in badList:
        try:
            assert(not isDec(item))
        except AssertionError:
            print("Assertion failed for dec at: {}".format(item))


if __name__ == "__main__":
    # hexUnit()
    # binUnit()
    # decUnit()
    sys.stdout.write("Enter number: ")
    sys.stdout.flush()


    number = sys.stdin.readline().strip().replace(' ','')
    
    # handle error
    n_type = inputCheck(number)

    if(n_type == DEC):
        parsed_number = decStringToNumber(number)
    elif(n_type == HEX):
        parsed_number = hexStringToNumber(number)
    elif(n_type == BIN):
        parsed_number = binStringToNumber(number)

    if(parsed_number is None):
        print("Input wasn't in valid dec, hex or bin format")
        sys.exit(1)


    # handle error
    n_bytes = baseCheck(parsed_number)

    if(n_bytes == 1):
        __binstring=numberToBin(parsed_number)
        print("B: 0b{}".format(__binstring))
        print("H: 0x{}".format(binStringToHexString(__binstring)))
        print("D: {}".format(parsed_number))
        sys.exit()

    binlist=[list(), ]
    
    _tmplist=list()
    for i in range(1, n_bytes): 
        binlist[0].append("1")
        binlist.append(["1","0","X","X","X","X","X","X"])

    binlist[0].append("1")
    binlist[0].append("0")

    for i in range(n_bytes+1, 8):
         binlist[0].append("X")
        
    dummy_list = list()
    for item in binlist:
        for char in item:
            dummy_list.append(char)

    bin_form = [char for char in bin(parsed_number)[2:]]
    dummy_list.reverse()
    str_index=0
    while(len(bin_form) > 0):
        str_index = dummy_list.index("X")
        dummy_list[str_index]=bin_form.pop()
        # dummy_list[bin_form.pop()]=str_index
    while True:
        try:
            str_index = dummy_list.index("X")
            dummy_list[str_index]="0"
        except ValueError:
            break
    dummy_list.reverse()
    bin_str="".join(dummy_list)
    __finalnumber = binStringToNumber(bin_str)

    print("B: 0b{}".format(bin_str))
    print("H: 0x{}".format(binStringToHexString(bin_str)))
    print("D: {}".format(parsed_number))



    
