#!/usr/bin/env python3

""" script for number conversion """

import sys

NUMBER_DICT = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "A": 10,
    "a": 10,
    "B": 11,
    "b": 11,
    "C": 12,
    "c": 12,
    "D": 13,
    "d": 13,
    "E": 14,
    "e": 14,
    "F": 15,
    "f": 15
}

BIN_CHARS = ("0", "1")
DEC_CHARS = BIN_CHARS + ("2" ,"3" ,"4" ,"5" ,"6" ,"7", "8", "9")
__HEX_CHARS = ("A", "B", "C", "D", "E", "F")
HEX_CHARS = DEC_CHARS + __HEX_CHARS + tuple(item.lower() for item in __HEX_CHARS)

def isHex(string):
    # TODO optimization
    
    if(string[0:2] == "0x" or string[0:2] == "0X"):
        __string = string[2:]
    else:
        __string = string

    for l in __string:
        if (l not in HEX_CHARS):
            return False

    return True

def isBin(string):

    if(string[0:2] == "0b" or string[0:2] == "0B"):
        __string = string[2:]
    else:
        __string = string

    for c in __string:
        if(c not in BIN_CHARS):
            return False
    
    return True

def isDec(string):
    for c in string:
        if(c not in DEC_CHARS):
            return False
    
    return True

def hexCharToBin(char, padding=True):
    # NE RABIM
    if(char not in HEX_CHARS):
        raise ValueError("Can't convert {} from hex".format(char))

    number = NUMBER_DICT[char]
    binlist=list()
    while(number > 0):
        val = number & 1
        # Infinite loop je tu

    if(padding and len(binlist) < 4):
        while(len(binlist) < 4):
            binlist.append("0")
    binlist.reverse()
        
    return "".join(binlist)

def decToBin(string, padding=True):
    # NE RABIM
    for c in string:
        if(c not in DEC_CHARS):
            raise ValueError("Not a valid number")

    number = int(string)
    binlist=list()
    while(number > 0):
        val = number & 1
        binlist.append(str(val))
        number = number >> 1
    return hexToBin(char, padding)

def __numberToBin(number, zeropadding=8):
    __number=number
    binlist=list()
    while(__number > 0):
        if(__number & 1):
            binlist.append("1")
        else:
            binlist.append("0")
        __number = __number >> 1
    while(len(binlist) < zeropadding):
        binlist.append("0")
    binlist.reverse()
    return "".join(binlist)

def numberToBin(number):
    return __numberToBin(number)

def hexCharToBin(char, padding=True):
    return __numberToBin(NUMBER_DICT[char], 4)

def hexCharToNumber(char):
    return NUMBER_DICT[char]

def decStringToNumber(decString):
    binlist=[char for char in decString]
    counter = 0
    value = 0
    while(len(binlist) > 0):
        value = value + NUMBER_DICT[binlist.pop()]*(10**counter)
        counter = counter + 1
    
    return value

def decStringToBin(decString):
    return (__numberToBin(decStringToNumber(decString)))

def hexStringToNumber(hexString):
    binlist=[char for char in hexString]
    counter = 0
    value = 0
    while(len(binlist) > 0):
        value = value + NUMBER_DICT[binlist.pop()]*(16**counter)
        counter = counter + 1
    
    return value


def hexStringToBin(hexString):
    binstring=str()
    for char in hexString:
        binstring = binstring + hexCharToBin(char)    
    return binstring

def binStringToNumber(string):
    __string = [char for char in string]
    __string.reverse()
    counter=0
    value=0
    for char in __string:
        value = value + NUMBER_DICT[char]*(2**counter)
        counter = counter + 1
    return value


def binStringToHexString(binstring):
    __binstring = [c for c in binstring]
    __binstring.reverse()

    while(len(__binstring) % 4 != 0):
        __binstring.append("0")

    return_string=""

    while(len(__binstring) > 0):
        buffer=""
        __temp_list=list()
        for i in range(0, 4):
            __temp_list.append(__binstring.pop())
        number = binStringToNumber("".join(__temp_list))
        return_string = return_string + HEX_CHARS[number]

    return return_string

def hexToBin(string):
    buffer = list()
    for letter in string:
        buffer.append(hexCharToBin(letter))
    
    return "".join(buffer)

def decToBin(string):
    return hexToBin(string)


def binToHex(string):
    number = int(string, 2)
    return hex(number)[2:]

def binToDec(string):
    return int(string, 2)

if __name__ == "__main__":
    print(OtherbinStringToHexString("11101010"))
    